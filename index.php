<!doctype html>
<html lang="en">
<head>
  <title>LOVED BY KIDS</title>

     <!-- WEBAPP META INFO : APP ICONS -->
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="apple-touch-icon-precomposed" href="imgs/appico.png"/> 

    <!-- FONTS AND STYLES -->
    <link rel="stylesheet" href="css/kids.css">

</head>
<body>

<div id="wrapper">


  <!-- STATIC -->
  <div id="spiral"></div>
  <div id="viewport"></div>


  <!-- UI ===================================== -->
  <div id="fade"></div>
  <div id="fadeTop"></div>
  <div id="btn-restart"></div>
  <div id="patchCharater"></div>
  <div id="charact1"></div> 
  <div id="charact2"></div>
  <div id="charact3"></div>
  <div id="charact4"></div>
  <div id="tabs">
      <div id="pg1"></div>
      <div id="pg2"></div>
      <div id="pg3"></div>
  </div>
 

  <!-- SCREENS ===================================== -->
  <section id="intro-animation"></section> 
  
  <!-- QUESTION S :::::::::::::::::::::::::::::::::: -->
  <div id="q"></div>

  <!-- ANSWER 1 :::::::::::::::::::::::::::::::::: -->
  <div id="a"></div>

  <!-- FORM ENTRY :::::::::::::::::::::::::::::::: -->


</div>

<!-- SCRIPTS -->
<script data-main="js/main" src="js/lib/require.js"></script>

</body>
</html>