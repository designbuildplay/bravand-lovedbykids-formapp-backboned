
// ==============================
// THE MAIN APPLICAIOTN LOGIC .
// ==============================

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),

        Router      = require('router'),
        
        Question      = require('model.question'),
        Questions     = require('collection.questions'),
        Answers       = require('collection.answers'),
       
        QuestionsList = require('view.question-list'),
        AnswersList = require('view.answer-list');

    var initialize = function(){
  
    // Pass in our Router module and call it's initialize function
  
    var appRouter = new Router();  //define our new instance of router   
    Backbone.history.start();   // use # History API
    //Backbone.history.start({pushState: true, root: "/work/2014/Bravand/LovedByKids/lbk-backboned/"});   // use html5 History API
    
    console.log("its up")

    

    // var view_questions = new QuestionsList({collection: Questions});
    // view_questions.render();
    // //console.log(view_questions.collection.models)
    // view_questions.collection.models[0].set({"char":"char1"});

    // var view_answers = new AnswersList({collection: Answers});
    // view_answers.render();

    // return appRouter
  }


  return {
    initialize: initialize

  };

});
