define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Answer     = require('model.answer');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PrjAs = Backbone.Collection.extend({
      model:Answer,
      url:"/"
    });

    var Answers = new PrjAs([
      { id: 1, title: "YOU ANSWERED", desc: "of teachers asked said they were NOT CONFIDENT at using technology in the classroom.", percent:"49", note:"Story Station has been built with teachers in mind, making it easy for both you and your classroom to use it right from day one!" },
      { id: 2, title: "YOU ANSWERED", desc: "of teachers asked said BUDGET was the biggest hold-back for them using new technology in their classroom", percent:"70", note:"At £400 per year including all updates and new functionality, Story Station offers great functionality at a great value price." },
      { id: 3, title: "YOU ANSWERED", desc: "of teachers asked said that technology will play a BIGGER role in their classroom in the future.", percent:"90", note:"Why not ask one of our staff about how Story Station can help you combine ICT with offline activities and lesson plans."  } 
    ]); 


    // Return the collection
    return Answers;

});