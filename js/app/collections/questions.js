define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Question     = require('model.question');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PrjQs = Backbone.Collection.extend({
      model:Question,
      url:"/"
    });

    var Questions = new PrjQs([
      { id: 1, title: "Question 1", desc: "How confident are you at using technology devices in the classroom?", Q1:"Very confident", Q2:"I'm ok", Q3:"Not confident" },
      { id: 2, title: "Question 2", desc: "What do you think is the biggest hold-back for teachers who want to use new technology in the classroom?", Q1:"Budget", Q2:"Lack of Training / <br>Expertise", Q3:"Resistance from <br>School Management" },
      { id: 3, title: "Question 3", desc: "Do you think that technology will play a bigger or smaller role in your classroom in the future?", Q1:"Bigger role", Q2:"Smaller role", Q3:"About the same" } 
    ]); 


    // Return the collection
    return Questions;

});