// QUESTION ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        Answers            = require('collection.answers'),
        AnswersList        = require('view.answer-list'),
        projectTemplate    = require("text!../templates/question.html");

     var view_answers = new AnswersList({collection: Answers});
     var anwser_model = null;
     console.log("ans ", view_answers.collection.model[0])

    // CONTENT :::::::::::::::::::::::::::::::::::

    var Q1View = Backbone.View.extend({
        tagName:'div',
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            // 'click': 'clickTag',
            'click #q1a': 'clickTag',
            'click #q1b': 'clickTag',
            'click #q1c': 'clickTag',
        },
        
        clickTag:function(ev){
               
                var answer_letter = $(ev.target).data('answer');
                console.log("clicky ", answer_letter)
                this.model.set('answer',answer_letter)
                //console.log("you hit ", this.model.get('answer'))
                view_answers.collection.models[anwser_model].set({"answer":answer_letter});
                view_answers.render(anwser_model); // call render Answer with model reference ID
        },
    

        render:function (model) {
          anwser_model = model;
          console.log("q model ", this.model.id)
          var content = this.template( this.model.toJSON())
          $(this.el).append(content);

          if( $(this.el).append(content) ){
                TweenLite.fromTo("#Q1", 2, {y:-800, x:0}, {y:0, overwrite:true })
                console.log("ready")
            }
         // TweenLite.fromTo("#Q1", 2, {y:-800, x:0}, {y:0})
        }
    });


    // Our module now returns our view
    return Q1View;


});
