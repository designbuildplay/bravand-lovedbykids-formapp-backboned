// THE HOMEPAGE GRID VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),

        //App                = require('app'),
        //Router             = require('router'),

        projectTemplate    = require("text!../templates/welcome.html");

    //var scope = this;

    // CONTENT :::::::::::::::::::::::::::::::::::

    var Welcome = Backbone.View.extend({
        //collection: Projects,
        scope: this,
        tagName:'div',
        id:"welcome",    
        el:'#viewport',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click .btn-welcome': 'welcomeClick',
             // 'dblclick': 'clickTag',
            // 'keypress .edit': 'updateOnEnter',
            // 'blur .edit':   'close'
        },

        initialize:function () {
              console.log("view active welcome screen" )
              this.render();
        },
      
       welcomeClick:function(){
            var scope = this;
            TweenMax.to('#welcome',  0.5, { alpha:0, scale:0.5, overwrite:true, onComplete: scope.intro });
        },

        intro: function(){
            var Router = require('router'),
                appRouter = new Router(); 

            appRouter.navigate('intro', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
        },

        render:function () {
            this.$el.html(this.template());
            return this;
        }
    });


    // Our module now returns our view
    return Welcome;

});
