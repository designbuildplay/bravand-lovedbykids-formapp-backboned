// THE INTRO VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        Canvas             = require('canvas'),

        //App                = require('app'),
        //Router             = require('router'),

        projectTemplate    = require("text!../templates/intro.html");

    //var scope = this;

    // CONTENT :::::::::::::::::::::::::::::::::::

    var Intro = Backbone.View.extend({

        tagName:'div',
        id:"intro",    
        el:'#intro-animation',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            //'click .btn-welcome': 'welcomeClick',
             // 'dblclick': 'clickTag',
            // 'keypress .edit': 'updateOnEnter',
            // 'blur .edit':   'close'
        },

        initialize:function () {
              console.log("view active intro animation" )
              this.render();
        },
      
        intro: function(){
            var Router = require('router'),
                appRouter = new Router(); 
            
            appRouter.navigate('intro', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
        },

        render:function () {
            this.$el.html(this.template());
            var canvasani = new Canvas()

            return this;
        }
    });


    // Our module now returns our view
    return Intro;

});
