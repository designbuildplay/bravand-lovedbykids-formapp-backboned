// ANSWER LIST VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),

       AnsView = require('view.answer');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var AListView = Backbone.View.extend({
        //collection: Projects,
        tagName:'div',
        id:"question",    
        el:'#viewport',   //selects element rendering to
      
        initialize: function(){
            _.bindAll(this, "renderItem");
        },
        
        renderItem: function(model){

            var ansItem = this.collection.models[model]
            var ansView = new AnsView({model: ansItem});  // CREATE A VIEW FOR EACH MODEL ITEM
            ansView.render();
            $(this.el).append(ansView.el);
        },
        
        render: function(id){
          console.log("view render answer list" )
          //this.collection.each(this.renderItem);
          this.renderItem(id)
        }
    });


    // Our module now returns our view
    return AListView;


});
