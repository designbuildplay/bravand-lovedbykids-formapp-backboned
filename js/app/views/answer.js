// QUESTION ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        projectTemplate    = require("text!../templates/answer.html");

    var sect;

    // CONTENT :::::::::::::::::::::::::::::::::::

    var AView = Backbone.View.extend({
        tagName:'div',
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            'click .btnNext': 'nextHit',
            'mouseover .btnNext': 'nextOver',
            'mouseout .btnNext': 'nextOut'
        },
          
        nextHit:function(ev){  
             var scope = this;
               console.log("BOOOM")
               TweenLite.to("#A1", 2, {y:1200, x: -400});
               scope.sceneMove()
        },

        nextOver:function(ev){  
               TweenMax.to('#btnNext',  0.5, { scale:1.1, ease:Back.easeOut});
        },

        nextOut:function(ev){  
              TweenMax.to('#btnNext',  0.2, { scale:1});
        },

        render:function () {
          console.log("Ans model ", this.model.id)
          sect = this.model.id;
          //this.$el.html(this.template( this.model.toJSON() ));
          var html = this.template( this.model.toJSON())
          $(this.el).append(html);

          TweenLite.to("#Q1", 2, {y:1200, x: 400});
         // TweenLite.fromTo('#A1', 0.5, {y:-800}, {y:0,, x:0, delay:0.4 });
        },

        sceneMove:function(){
            var Router = require('router'),
                appRouter = new Router(); 

                switch(sect)
                {
                case 1:
                  appRouter.navigate('question2', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
                  break;
                case 2:
                  appRouter.navigate('question3', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
                  break;
                case 3:
                  appRouter.navigate('form', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
                  break;
                }
        }

    });


    // Our module now returns our view
    return AView;


});
