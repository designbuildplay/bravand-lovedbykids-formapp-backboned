// QUESTION LIST VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),

       QuesView = require('view.question');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var QListView = Backbone.View.extend({

        tagName:'div',
        id:"question",    
        el:'#viewport',   //selects element rendering to
      
        initialize: function(){
            _.bindAll(this, "renderItem");
        },
        
        renderItem: function(model){
            //console.log("mod " + model)
            var modelItem = this.collection.models[model]
            var qView = new QuesView({model: modelItem});  // CREATE A VIEW FOR EACH MODEL ITEM
            qView.render(model);

            $(this.el).append(qView.el);
        },
        
        render: function(id){
           console.log("view render question list", id )
           //this.collection.each(this.renderItem);

           this.renderItem(id)
        }
    });


    // Our module now returns our view
    return QListView;


});
