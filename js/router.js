
// ========
// ROUTER : 
// ========

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES :::::::::::::::::::

    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),

        Welcome     = require('view.welcome'),
        Intro       = require('view.intro'),
        Question       = require('view.question'),
        Questions     = require('collection.questions'),
        Answers       = require('collection.answers'),
        QuestionsList = require('view.question-list'),
        FormView = require('view.form');
        

    var view_questions = new QuestionsList({collection: Questions});

    //var questions_collection = new Questions();
    //console.log("coll is ", Questions.models[0])
    // //console.log(view_questions.collection.models)
    // view_questions.collection.models[0].set({"char":"char1"});

   //define router class
    var AppRouter = Backbone.Router.extend ({
        routes: {
            '' : 'home',
            'intro' : 'intro',
            'question1': 'q1',
            'question2': 'q2',
            'question3': 'q3',
            'form': 'form'
        },

        home: function () {
            console.log('you are viewing home page');
            var welcome_view = new Welcome(); // WELCOME SCREEN
            // 
        },
        
        intro: function () {
            console.log('you are viewing a intro');
            var intro_view = new Intro();
        },

        q1: function () {
            console.log('you are viewing a question1');
            view_questions.render(0); // call render funct with model reference ID
        },

        q2: function () {
            console.log('you are viewing a question2');
            view_questions.render(1); // call render funct with model reference ID
        },

        q3: function () {
            console.log('you are viewing a question3');
            view_questions.render(2); // call render funct with model reference ID
        },

        form: function () {
            console.log('you are viewing the form');
            var form_view = new FormView();
        },



        initialize: function(){
            console.log("route this thing")
        }

    });

  return AppRouter

});
