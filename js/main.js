
// Script loader (RequireJS) ==========================

require.config({

  //'baseUrl': '/DBP/dbp_phase2/dbp_p2_build/js/', // THE BASE URL

   paths: {
        /* jQuery */
        'jquery': 'lib/jquery-1.7.2.min',
        'underscore': 'lib/underscore-min',
        'backbone': 'lib/backbone-min',

       

        'tweenmax': 'lib/gs/TweenMax.min',
        'ease': 'lib/gs/easing/EasePack.min',
        'jquery.form': 'lib/jquery.form',
        'pixi': 'lib/pixi/pixi.dev',
        'fastclick': 'lib/fastclick',

        /* APP MODULES */
        'app': 'app',
        'canvas': 'app/canvas/Canvas',
        'grafic': 'app/canvas/Grafic',
        'model.question' : 'app/models/question',
        'model.answer' : 'app/models/answer',
        'collection.questions' : 'app/collections/questions',
        'collection.answers' : 'app/collections/answers',
        'view.welcome': 'app/views/welcome',
        'view.question': 'app/views/question',
        'view.question-list': 'app/views/question-list',
        'view.answer': 'app/views/answer',
        'view.answer-list': 'app/views/answer-list',
        'view.intro': 'app/views/intro',
        'view.form': 'app/views/form',


         /* Router */
        'router': 'router',
    },

    shim: {

        'underscore': {
          'exports': '_'
        },

        'backbone': {
          'deps': ['jquery', 'underscore'],
          'exports': 'Backbone'
        },

        'fastclick': {
            exports: 'FastClick'
        },

        "tweenmax" : 
         {deps: ["jquery"],
          exports :"TweenMax"
         },

         'pixi': {
          exports: 'PIXI'
        }
 
   }
});


require([
  // Load our app module and pass it to our definition function
  
  'app',
  'jquery', 
  'backbone',
  //'router'

], function(App, $, Backbone){
  // The "app" dependency is passed in as "App"
    App.initialize();
    
});

