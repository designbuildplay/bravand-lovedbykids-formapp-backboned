
// © design.build.play. ltd =====================
// THE MAIN APP CONTROLLER ======================
var app;
var canvasani;

var App = function(){
	this.spiral = document.getElementById('spiral'),
	this.welcome = document.getElementById('welcome'),
	this.intro = document.getElementById('intro'),
	this.thankyou = document.getElementById('thankyou'),
	this.btnWelcome = document.getElementById('btn-welcome'),
	this.fade = document.getElementById('fade'),
	this.fadeTop = document.getElementById('fadeTop'),
	this.btnRestart = document.getElementById('btn-restart'),
	this.patchCharater = document.getElementById('patchCharater'),
	this.tabs = document.getElementById('tabs'),
	this.btnNext = document.getElementById('btnNext'),
	this.btnSubmit = document.getElementById('btnSubmit'),
	this.formBox = document.getElementById('formBox'),
	this.forminputs = document.getElementById('form'),
	this.quitPop = document.getElementById('quitPop'),
	this.termsPop = document.getElementById('termsPop'),
	this.quitNo = document.getElementById('quitNo'),
	this.quitYes = document.getElementById('quitYes'),
	this.char1 = document.getElementById('charact1'),
	this.char2 = document.getElementById('charact2'),
	this.char3 = document.getElementById('charact3'),
	this.char4 = document.getElementById('charact4'),
	this.Q1 = document.getElementById('Q1'),
	this.Q2 = document.getElementById('Q2'),
	this.Q3 = document.getElementById('Q3'),
	this.A1 = document.getElementById('A1'),
	this.A2 = document.getElementById('A2'),
	this.A3 = document.getElementById('A3'),
	this.firstPlay = true,
	this.email_good = false,
	this.delay = 3,
	this.q1answer = "",
	this.q2answer = "",
	this.q3answer = "",
	this.sect = 0,
	scope = this;

	$(scope.btnWelcome).click(function() {
	  	console.log("start the quiz")
	  	TweenMax.to(scope.spiral,  0.5, { alpha:0, overwrite:true});
	 	TweenMax.to(scope.welcome,  1, { alpha:0, scale:0.5, overwrite:true, onComplete:app.introAnimation});
	});

	$(scope.btnNext).click(function() {
	  	console.log("next hit")
	  	app.nextSect(scope.sect)
	});

	$(scope.btnNext).hover(
        function () {
           TweenMax.to(scope.btnNext,  0.5, { scale:1.1, ease:Back.easeOut});
         },
        function () {
           TweenMax.to(scope.btnNext,  0.2, { scale:1});
          }
    );

    $(scope.btnSubmit).hover(
        function () {
           TweenMax.to(scope.btnSubmit,  0.5, { scale:1.1, ease:Back.easeOut});
         },
        function () {
           TweenMax.to(scope.btnSubmit,  0.2, { scale:1});
          }
    );


	$(scope.btnRestart).click(function() {
	  	app.showReset()
	});

	$(scope.quitYes).click(function() {
	  	app.appReset()
	});

	$(scope.quitNo).click(function() {
	  	app.resetHide()
	});

	$('#termsLink').click(function() {
	  	app.showTerms()
	});

	$('#btnClose').click(function() {
	  	app.resetHide()
	});

	$('#form').ajaxForm( {
		target: '', 
		success: function() { 
			console.log("form submit")
	  		app.formSubmit()
		} 
	});
	$("input:checkbox").click(function() {
	    if ($(this).is(":checked")) { 
	     	 console.log("on")
	     	 if( scope.email_good == true){
	     	 	scope.btnSubmit.disabled = ''
	     	 	scope.btnSubmit.style.opacity = 1
	     	 }
	     	 

	   	 } else {
	        console.log("off")
	         scope.btnSubmit.disabled = 'disabled'
	         scope.btnSubmit.style.opacity = 0.5
	    }
	}); 

	this.init()
}


App.prototype.init = function(){
  	console.log("kids app init"  )
  	TweenMax.set('#tabs',  { y:400, overwrite:true});
  	TweenMax.set(scope.patchCharater,  { x:'-330%', overwrite:true});
  	this.btnRestart.style.display = "none"
  	scope.intro.style.opacity = 0;

  	TweenLite.fromTo(scope.spiral, 2, {alpha:0}, {alpha:1 });
  	TweenLite.fromTo(scope.spiral, 14, {rotation:0}, {rotation:100 });
  	TweenLite.fromTo(scope.welcome, 2, {rotation:0, scale:0.4,alpha:0,}, {rotation:0, scale:1, alpha:1, delay:0.4, ease:Back.easeOut });
  	TweenLite.fromTo('#logoInt', 1.5, {scale:0.2,alpha:0,}, {scale:1, alpha:1, delay:0.5, ease:Back.easeOut });
}


App.prototype.introAnimation = function(){
	console.log("play intro animation")
	scope.btnRestart.style.display = "inline";
	scope.btnWelcome.style.display ='none';
	scope.btnRestart.style.opacity = 0;
	scope.fade.style.display ='inline';

	if(scope.firstPlay == true){
	 	canvasani.init()
	 	scope.firstPlay = false
	 	}
	 else{
	 	canvasani.replay()
	 }

	TweenMax.to(scope.intro,  0.5, { alpha:1, overwrite:true});
	TweenLite.fromTo(scope.btnRestart, 0.5, {alpha:0}, {alpha:1});
	TweenMax.to(scope.patchCharater,  0.6, { x:'0%', delay:10, rotation:80, overwrite:true, ease:Back.easeOut});
	TweenMax.to(scope.fade,  0.5, { alpha:0.7, delay:8.5, onComplete:app.question1 });

}


App.prototype.nextSect = function(sect){
	scope.sect = sect + 1
	console.log(scope.sect)

	switch (scope.sect) {
	  case 2:
	    app.question2()
	    TweenMax.to(scope.patchCharater,  0.5, { rotation:90 });
	  break;
	  case 3:
	    app.question3()
	    TweenMax.to(scope.patchCharater,  0.5, { rotation:180 });
	  break;
	  case 4:
	    app.formIn()
	      // sets the values of quesitons to form fields
	      $("#q1input").val(q1answer);
	      $("#q2input").val(q2answer);
	      $("#q3input").val(q3answer);

	     TweenMax.to(scope.patchCharater,  0.5, { rotation:270 });
	  break;
	}

	TweenMax.to(scope.btnNext,  0.5, { x:900, overwrite:true});

}

App.prototype.question1 = function(){
	scope.sect = 1,
	a1 = document.getElementById('q1a'),
	a2 = document.getElementById('q1b'),
	a3 = document.getElementById('q1c'),
	scope.char1.style.display = "inline";
	scope.Q1.style.display = "inline";
	
	TweenMax.to('#tabs',  0.5, { y:0 });
	TweenMax.to(scope.Q1,  0.5, { alpha:1, overwrite:true});
	TweenLite.fromTo(scope.Q1, 2, {y:'-700%', scale:0.5}, {y:'0%', scale:1, ease:Power1.easeOut});
	TweenLite.fromTo(scope.char1, 1, {x:'-400%', scale:0.5}, {x:'0%', scale:1, delay:0.3});

	$(a1).click(function() {
	  	q1answer = "A"
	  	console.log("Q1 is ",q1answer)
	  	app.answer1()
	});

	$(a2).click(function() {
	  	q1answer = "B"
	  	console.log("Q1 is ",q1answer)
	  	app.answer1()
	});

	$(a3).click(function() {
	  	q1answer = "C"
	  	console.log("Q1 is ",q1answer)
	  	app.answer1()
	});

	$("#pg1").addClass("bgRed")
}

App.prototype.answer1 = function(){
	$('#a1result').html(q1answer)
	scope.A1.style.display = 'inline';
	scope.btnNext.style.display = 'inline';

	TweenLite.to(scope.Q1, scope.delay, {y:1200, x: 400});
	TweenLite.fromTo(scope.A1, scope.delay, {y:'-700%', scale:0.7}, {y:'0%', scale:1, delay:scope.delay/2});
	TweenLite.fromTo(scope.btnNext, 0.5, {scale:0}, {scale:1, delay:scope.delay});
}


App.prototype.question2 = function(){
	console.log("Q2 in")
	a1 = document.getElementById('q2a'),
	a2 = document.getElementById('q2b'),
	a3 = document.getElementById('q2c'),
	scope.Q1.style.display = "none";
	scope.Q2.style.display = "inline";
	scope.char2.style.display = "inline";

	TweenLite.fromTo(scope.char1, 2, {y:'0%'}, {y:'400%'});
	TweenLite.fromTo(scope.char2, 1, {x:'-400%', scale:0.5}, {x:'-50%', scale:1, delay:0.3});

	TweenLite.to(scope.A1, scope.delay, {y:1200, x: -400});
	TweenMax.to(scope.Q2,  0.5, { alpha:1 });
	TweenLite.fromTo(scope.Q2, scope.delay, {y:'-700%', scale:0.5}, {y:'0%', scale:1});


	$(a1).click(function() {
	  	q2answer = "A"
	  	console.log("Q2 is ",q2answer)
	  	app.answer2()
	});

	$(a2).click(function() {
	  	q2answer = "B"
	  	console.log("Q2 is ",q2answer)
	  	app.answer2()
	});

	$(a3).click(function() {
	  	q2answer = "C"
	  	console.log("Q2 is ",q2answer)
	  	app.answer2()
	});

	$("#pg1").removeClass("bgRed")
	$("#pg2").addClass("bgRed")
}

App.prototype.answer2 = function(){
	$('#a2result').html(q2answer)
	scope.A2.style.display = 'inline';

	TweenLite.to(scope.Q2, scope.delay, {y:1200, x: 400});
	TweenLite.fromTo(scope.A2, scope.delay, {y:'-700%', scale:0.7}, {y:'0%', scale:1, delay:scope.delay/2});
	TweenLite.fromTo(scope.btnNext, 0.5, {x:900}, {x:0, delay:scope.delay});
}


App.prototype.question3 = function(){
	console.log("Q3 in")
	a1 = document.getElementById('q3a'),
	a2 = document.getElementById('q3b'),
	a3 = document.getElementById('q3c'),
	scope.Q2.style.display = "none";
	scope.Q3.style.display = "inline";
	scope.char3.style.display = "inline";
	TweenLite.fromTo(scope.char2, 2, {y:'0%'}, {y:'400%'});
	TweenLite.fromTo(scope.char3, 1, {x:'-400%', scale:1}, {x:'0%', scale:1, delay:0.3});

	TweenLite.to(scope.A2, scope.delay, {y:1200, x: -400});
	TweenMax.to(scope.Q3,  0.5, { alpha:1 });
	TweenLite.fromTo(scope.Q3, scope.delay, {y:'-700%', scale:0.5}, {y:'0%', scale:1});


	$(a1).click(function() {
	  	q3answer = "A"
	  	console.log("Q3 is ",q3answer)
	  	app.answer3()
	});

	$(a2).click(function() {
	  	q3answer = "B"
	  	console.log("Q3 is ",q3answer)
	  	app.answer3()
	});

	$(a3).click(function() {
	  	q3answer = "C"
	  	console.log("Q3 is ",q3answer)
	  	app.answer3()
	});

	$("#pg2").removeClass("bgRed")
	$("#pg3").addClass("bgRed")
}

App.prototype.answer3 = function(){
	$('#a3result').html(q3answer)
	scope.A3.style.display = 'inline';
	TweenLite.to(scope.Q3, scope.delay, {y:1200, x: 400});
	TweenLite.fromTo(scope.A3, scope.delay, {y:'-700%', scale:0.7}, {y:'0%', scale:1, delay:scope.delay/2});
	TweenLite.fromTo(scope.btnNext, 0.5, {x:900}, {x:0, delay:scope.delay});
}


// FORM FUNC IN ==============================
App.prototype.formIn = function(){
	scope.Q1.style.display = "none";
	scope.Q2.style.display = "none";
	scope.Q3.style.display = "none";
	scope.formBox.style.display = "inline";
	scope.btnSubmit.style.display = "inline";
	scope.char4.style.display = "inline";
	TweenLite.to('#tabs', 0.7, { y: 400 });
	TweenLite.fromTo(scope.char3, 1, {y:'0%'}, {y:'400%'});
	TweenLite.fromTo(scope.char4, 1, {x:'-400%', scale:0.5}, {x:'0%', scale:1, delay:0.3});
	TweenLite.to(scope.A3, scope.delay, {y:1200, x: -400});

	TweenLite.fromTo(scope.formBox, scope.delay, {y:'-700%', scale:0.7}, {y:'0%', scale:1, delay:scope.delay/2});
	TweenLite.fromTo(scope.btnSubmit, 0.5, {scale:0}, {scale:1});
}

App.prototype.formSubmit = function(){
	scope.thankyou.style.display = "inline";

	TweenLite.fromTo(scope.char4, 1, {y:'0%'}, {y:'400%'});
	TweenLite.to(scope.patchCharater, 0.7, { x: -800});
	TweenLite.to(scope.fade, 0.7, { alpha: 0});
	TweenLite.to(scope.btnRestart, 0.3, { alpha: 0});
	TweenLite.to(scope.formBox, scope.delay, {y:1200, x: 0});
	TweenLite.fromTo(scope.btnSubmit, 0.5, {scale:2}, {scale:0});

	TweenLite.to(scope.intro, 1.7, { scale: 0 });
	TweenLite.to(scope.spiral, 0.7, { alpha: 1, delay:0.5});
	TweenLite.fromTo(scope.thankyou, 0.5, {alpha:0, scale:0.5}, {alpha:1, scale:1, delay:0.8});

	TweenLite.to(scope.spiral, 5, { rotation: 110 });
	TweenLite.to(scope.spiral, 0.7, { alpha: 0,  delay:5.5});
	TweenLite.to(scope.thankyou, 0.7, { alpha: 0, scale:0.5, delay:5.5, onComplete:scope.restartApp});

}

App.prototype.showTerms = function(){
	scope.termsPop.style.display = "inline";
	TweenLite.to(scope.fadeTop, 0.7, { alpha:0.4});
	TweenLite.fromTo(scope.termsPop, 0.2, {scale:0}, {scale:1, ease:Back.easeOut});
}

// RESET FUNC   ==============================

App.prototype.showReset = function(){
	scope.fadeTop.style.display = 'inline'
	scope.quitPop.style.display = 'inline'
	TweenLite.to(scope.fadeTop, 0.7, { alpha:0.4});
	TweenLite.fromTo(scope.quitPop, 0.5, {scale:0}, {scale:1, delay:0.3, ease:Back.easeOut});
}

App.prototype.appReset = function(){
	//location.reload();
	scope.resetHide()
	scope.restartApp()
}


App.prototype.resetHide = function(){
	TweenLite.to(scope.fadeTop, 0.7, { alpha:0});
	TweenLite.fromTo(scope.quitPop, 0.2, {scale:1}, {scale:0});
	TweenLite.fromTo(scope.termsPop, 0.2, {scale:1}, {scale:0});
}


App.prototype.restartApp = function(){
	scope.fade.style.display ='none';
	scope.Q1.style.display = "none";
	scope.Q2.style.display = "none";
	scope.Q3.style.display = "none";
	scope.A1.style.display = "none";
	scope.A2.style.display = "none";
	scope.A3.style.display = "none";
	scope.formBox.style.display = "none";
	scope.char1.style.display = 'none';
	scope.char2.style.display = 'none';
	scope.char3.style.display = 'none';
	scope.char4.style.display = 'none';
	scope.fade.style.opacity = 0;
	scope.fadeTop.style.opacity = 0;
	scope.btnWelcome.style.display = "block";
	scope.btnNext.style.display = 'none';

	scope.q1answer = "";
	scope.q2answer = "";
	scope.q3answer = "";
	scope.sect = 0;
	scope.email_good = false;
	TweenLite.set(scope.Q1, {x:'0%', overwrite:true});
	TweenLite.set(scope.Q2, {x:'0%', overwrite:true});
	TweenLite.set(scope.Q3, {x:'0%', overwrite:true});
	TweenLite.set(scope.A1, {x:'0%', overwrite:true});
	TweenLite.set(scope.A2, {x:'0%', overwrite:true});
	TweenLite.set(scope.A3, {x:'0%', overwrite:true});
	TweenLite.set(scope.fade, {alpha:0, overwrite:true});
	TweenLite.set(scope.btnNext, {x:0, overwrite:true});
	
	TweenLite.set(scope.char1, {y:'0%', overwrite:true});
	TweenLite.set(scope.char2, {y:'0%', overwrite:true});
	TweenLite.set(scope.char3, {y:'0%', overwrite:true});
	TweenLite.set(scope.char4, {y:'0%', overwrite:true});
	TweenLite.set(scope.intro, { scale: 1, overwrite:true });

	scope.btnSubmit.disabled = 'disabled'
	scope.btnSubmit.style.opacity = 0.5
  	$("#c1").removeAttr("checked");
  	scope.forminputs.reset();

  	$("#pg1").removeClass("bgRed")
	$("#pg2").removeClass("bgRed")
	$("#pg3").removeClass("bgRed")

	scope.init()
	console.log("restart the app *")
}



App.prototype.validate = function(){
	
	
	var email = $("#email").val();
	console.log("VALID??! * ", email)
	 if (validateEmail(email)) {
			    $("#valid").text(" ");
			    scope.email_good = true;

			    if($('#c1').is(":checked")){
			    	scope.btnSubmit.disabled = ''
	     	 		scope.btnSubmit.style.opacity = 1
			    }
			    
		    //
		  } else {
		    $("#valid").text("* email is not valid");
		    scope.email_good = false;

		    scope.btnSubmit.disabled = 'disabled'
	     	scope.btnSubmit.style.opacity = 0.5
		  }
		
	return false;
	
}

// EMAIL VALIDATION ===========================
function validateEmail(email) {   
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
}

//prevent default scrolly ====================
document.ontouchmove = function(event){ 
    event.preventDefault(); 
}


// ===========================================
// init the app // ===========================

$(document).ready(function(){

        app = new App() // init the app

		//make FULL SCREEN on click of container ===================================
		document.getElementById('wrapper').addEventListener('click', function() {
		  var el = document.documentElement;
		      // rfs = el.webkitRequestFullScreen;
		      // rfs.call(el);

		      launchFullScreen(el);

		      function launchFullScreen(element) {
			  if(element.requestFullScreen) {
			    element.requestFullScreen();
			  } else if(element.mozRequestFullScreen) {
			    element.mozRequestFullScreen();
			  } else if(element.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT)){
			    element.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
			  }
			  setTimeout(function() {
			    if (!document.webkitCurrentFullScreenElement && element.webkitRequestFullScreen()) {
			      element.webkitRequestFullScreen();
			    }
			  },100);
			}

		});



	// BIND FASTCLICK TO THE DELAYS ==============
	window.addEventListener('load', function () {
		FastClick.attach(document.body);
	}, false);

});
